import { ethers } from "hardhat";

async function main() {
  // Obtenemos una instancia del contrato
  const Token = await ethers.getContractFactory("ERC20_ArqBC")
  // Disparamos una transaccion de para deployar una nueva instancia
  const token = await Token.deploy()
  // Esperamos a que se confirme el deploy
  await token.deployed()

  console.log("Token deployado en ", token.address)
}

main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
