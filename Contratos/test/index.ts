import { expect } from "chai"
import { ethers } from "hardhat"

describe("ERC20_ArqBC", function () {
  it("Token supply 0", async function () {
    const Token = await ethers.getContractFactory("ERC20_ArqBC")
    const token = await Token.deploy()
    await token.deployed()

    expect(await token.totalSupply()).to.equal(0)
  });
});
