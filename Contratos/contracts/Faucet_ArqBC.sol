// SPDX-License-Identifier: MIT
pragma solidity ^0.8.4;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";

interface IERC20_ArqBC is IERC20 {
    function mint(address to, uint256 amount) external;
}
contract Faucet_ArqBC {
    IERC20_ArqBC public token;

    constructor(IERC20_ArqBC token_) {
        token = token_;
    }

    function dameToken() external {
        require(token.balanceOf(msg.sender) == 0, "Tiene saldo");
        // https://docs.soliditylang.org/en/latest/units-and-global-variables.html?highlight=units#ether-units
        token.mint(msg.sender, 1 ether);
    }
}
