import * as assert from "assert";
import {ethers} from "ethers";

describe('Preguntas de la unidad', function () {
    it("Calcula el address para el nonce de la cuenta 70 para RSK en BIP44 usando la mnemotécnica 'hybrid echo bamboo save later phone absent enroll minimum survey silent depend'", ()=>{
        let wallet:ethers.Wallet
        assert.equal(wallet.address, "0x512cd13BD42AD45daA1B6b4D1eCd44d222792Bc0")
    })
    it("Utilizando la wallet con privatekey '0xc3954c7841c6d7c1b0505e22e972dafb23b9d00f84ebb20b729b8f561e8a36c5' firmar el mensaje 'Satoshi son los padres' y .", async ()=>{
        // Adicionalmente podes validarlo en https://etherscan.io/verifiedSignatures#
        let respuesta:string
        assert.equal(respuesta, "0xe86aa720c9a98e764657d490b6fff3a8a2ec82420c216eded221f04d3a4e032645e9fbcedb38c012b334006c89a23dd89c54abe68f8d6faee7b4b4eb7c31638f1c")
    })
});