import * as assert from "assert";
import {ethers} from "ethers";

describe('Preguntas de la unidad', function () {
    it("¿Con que otro nombre es conocido al algoritmo de hash Keccak-256?", ()=>{
        let respuesta:string = ''
        assert.equal(ethers.utils.solidityKeccak256(['string'], [respuesta.toLowerCase()]), "0xfd184dff27e73a6d6af165afdf47c5d20d7f3b8596d45ecee61004412ac0012d")
    })
    it("¿Cual es numero de BIP que define la forma de armar un backup con mnemotécnicas?", ()=>{
        let respuesta:number = 0
        assert.equal(ethers.utils.solidityKeccak256(['uint'],[respuesta]), "0x98a476f1687bc3d60a2da2adbcba2c46958e61fa2fb4042cd7bc5816a710195b")
    })
});