import * as ethers from "ethers"
import * as bip39 from "bip39"

/**
 * Esta función nos va a retornar una mnemotécnica de 12 palabras
 */
export function createSeedPhrase() {
    return bip39.generateMnemonic()
}

/**
 *
 * @param mnemonic Las palabras que dan origen a la wallet
 * @param path El path de derivación de la llave maestra [+info](https://medium.com/myetherwallet/hd-wallets-and-derivation-paths-explained-865a643c7bf2)
 */
export function getWallet(mnemonic: string, path: string) {
    return ethers.Wallet.fromMnemonic(mnemonic, path)
}

/**
 * Las wallet determinísticas jerárquicas o Hierarchal Desterministic Wallets (HD Wallets) nos permiten generar muchísima cantidad de
 * wallet (2^31) ^ 3 en base a un juego de palabras.
 * [+info](https://github.com/bitcoin/bips/blob/master/bip-0032.mediawiki)
 * @param mnemonic
 */
function getHDNode(mnemonic: string) {
    return ethers.utils.HDNode.fromMnemonic(mnemonic)
}

/**
 * La xPub o llave pública extendida es una llave maestra que nos permite calcular todas las llaves públicas que se
 * derivada de las palabras definidas.
 * [+info](https://github.com/bitcoin/bips/blob/master/bip-0032.mediawiki#Extended_keys)
 */
function getXPub(hdNode: ethers.utils.HDNode, path: string) {
    return hdNode.derivePath(path).neuter()
}

/**
 * La xpub es una llave **PRIVADA** maestra que nos permite precalcular todas las llaves privadas que se pueden generar
 * con las palabras definidas
 */
function getXPriv(hdNode: ethers.utils.HDNode, path: string) {
    return hdNode.derivePath(path)
}

// [+info](https://arshbot.medium.com/hd-wallets-explained-from-high-level-to-nuts-and-bolts-9a41545f5b0)
/**
 * Propósito, la primer parte de nuestro path de derivación, define el formato en el que usaremos la HD Wallet
 * en este caso usaremos BIP44, por lo tanto pondremos 44 como propósito
 */
const Purpose = "m/44'"
/**
 * Tipo de moneda, en esta parte del path de derivación se coloca el codigo de la blockchain para la cual queremos
 * calcular el juego de llaves, en este caso vamos a usar 60 que es el codigo para Ethereum, para ver la lista completa
 * de tipos de moneda visita https://github.com/satoshilabs/slips/blob/master/slip-0044.md
 */
const CoinType = "/60'"
/**
 * Cuenta, esta es la ultima parte requerida para poder conseguir una llave extendida que nos permita calcular las derivas
 * comieza por 0 y puede llegar a 2^31, 2.147.483.648 cuentas
 */
const Account = "/0'"

// Creamos un juego de palabras
const seed = createSeedPhrase()
console.log({seed})
// Preparamos el nodo maestro para comenzar a derivar
const hdNode = getHDNode(seed)

// Calculamos la llave extendida que daran pie a los 2 billones de wallet
const pathAccount0 = Purpose+CoinType+Account // m/44'/60'/0'
const xPub = getXPub(hdNode, pathAccount0)

// ahora generamos por ejemplo la wallet 1.021.894.002
const publica = xPub.derivePath('1021894002')
console.log({address: publica.address})

// Todo esto sin haber tocado la privada, vamos a generarla
const xPriv = getXPriv(hdNode, pathAccount0)
const privada = xPriv.derivePath('1021894002')
console.log({privada: privada.privateKey})

if (publica.address === privada.address) {
    console.log("Es la misma wallet")
}

/**
 * Si quisieramos guardar esta cuenta recién generada en un archivo o una base da datos, lo más recomendado es usar
 * el KeyStore estandar que se genera desde la wallet.
 */
// Generamos una wallet desde la privateKey
const wallet = new ethers.Wallet(privada.privateKey);

async function exportarYFirmar(wallet: ethers.ethers.Wallet) {
    // La exportamos a un keyStore encriptado con password
    const keystoreEncriptado = await wallet.encrypt('UnPassword')
    console.log({'Keystore encriptado': keystoreEncriptado})

    /**
     * Por ultimo vamos a firmar un mensaje con la privada que generamos
     */
    const firma = await wallet.signMessage('Test')
    console.log({firma})
}

exportarYFirmar(wallet)

