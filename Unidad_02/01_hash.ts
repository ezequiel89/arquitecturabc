import {ethers} from "ethers"
import hexToBinary from "../tools/hexToBinary"

function sha256OfString(value: string): string {
    // Este comando nos permite convertir una cadena de texto en un vector de números,
    // donde cada numero representa un caracter en utf8 https://www.utf8-chartable.de/
    /**
     * ethers.utils.toUtf8CodePoints("ABC") = buffer: [65,66,67]
     */
    const buffer = ethers.utils.toUtf8CodePoints(value)
    return ethers.utils.sha256(buffer)
}

/**
 * Otra función de hash conocida es keccak256 tambien conocido con sha3, muy utilizada en Web3.
 * @param value
 */
export function keccak256OfString(value: string): string {
    const buffer = ethers.utils.toUtf8CodePoints(value)
    return ethers.utils.keccak256(buffer)
}

function solidityKeccak256OfString(value: string): string {
    // Esta función nos permite codificar los párametros de diferente tipo de la misma forma que lo hace
    // un contrato en EVM, al hacer el empaquetado de los parámetros en formato crudo.
    return ethers.utils.solidityKeccak256(['string'], [value])
}

/**
 * De esta forma podemos empaquetar los bytes de multiples variables de la misma forma en la que lo hace la
 * EVM (Ethereum Virtual Machine) logrando idéntico resultado al del contrato
 * @param valueString
 * @param valueNumber
 */
function solidityKeccak256OfMultipleArguments(valueString: string, valueNumber: number): string {
    return ethers.utils.solidityKeccak256(['string', 'uint256'], [valueString, valueNumber])
}

/**
 * Esta función ejemplifica el funcionamiento del minado en PoW
 * @param content: El contenifo del bloque
 * @param difficulty: La dificultad que se tiene que alcanzar
 * @param startAtNonce: El nonce por el cual va a comenzar este script
 * @param endAtNonce: El nonce donde este script va a finalizar, si es que no encuentra una solucion antes.
 */
function conceptProofOfWorkMining(content: any, difficulty: number, startAtNonce: number = 0, endAtNonce: number = 1000): {
    nonce: number,
    hash: string,
    hashBinary: string,
    isValid: boolean,
    elapsed: number
} {
    let nonce = startAtNonce
    let hash: string
    let hashBinary: string
    let isValid: boolean
    const startAtTimestamp = Date.now()
    const prefix = '0'.repeat(difficulty)
    do {
        nonce++
        const block = {
            nonce,
            content
        }
        hash = sha256OfString(JSON.stringify(block))
        hashBinary = hexToBinary(hash)
        isValid = hashBinary.startsWith(prefix)
    } while (!isValid || nonce == endAtNonce);
    const endAtTimestamp = Date.now()
    return {
        nonce,
        hash,
        hashBinary,
        isValid,
        elapsed: endAtTimestamp - startAtTimestamp
    }
}


/**
 * Al ejecutar este script vas a poder visualizar las salidas de cada una de las funciones.
 */
console.log({
    sha256: sha256OfString('UnaCadenaDeTexto'),
    keccak256: keccak256OfString('UnaCadenaDeTexto'),
    solidityKeccak256: solidityKeccak256OfString('UnaCadenaDeTexto'),
    solidityKeccak256MultipleArguments: solidityKeccak256OfMultipleArguments('UnaCadenaDeTexto', 123456),
    conceptProofOfWorkMining_d1: conceptProofOfWorkMining(
        ['Transaccion1', 'Transaccion2'],
        1
    ),
    conceptProofOfWorkMining_d5: conceptProofOfWorkMining(
        ['Transaccion1', 'Transaccion2'],
        5
    ),
    conceptProofOfWorkMining_d10: conceptProofOfWorkMining(
        ['Transaccion1', 'Transaccion2'],
        10
    )
})

// Para dar un poco de contexto, la dificultad actual de Bitcoin es de 27.452.707.696.466