import * as dotenv from "dotenv"
import { ethers } from "ethers"

dotenv.config()

// El provider nos sirve para poder comunicarnos con un nodo de la red al que entre otras cosas le podemos pedir:
// * Simular una transacción para estimar gas o conocer lo que retornaría en el estado actual que conoce
// * Consultar por evento pasados
// * Pedirle que comparta con el resto de la red una transacción para confirmar
// * Consultar estado general de toda la red y el nodo en particular
const provider = new ethers.providers.InfuraProvider("rinkeby");

/**
 * Con la siguiente función vamos a poder consultar el balance en moneda nativa de una cuenta
 */
function getBalance(address: string) {
    return provider.getBalance(address)
}

/**
 * Podemos ver el balance de esta cuenta en
 * https://rinkeby.etherscan.io/address/0x87b4f1f9eae6fa4e5ed228f48b911c0149ce48b1
 */
getBalance('0x87b4f1f9eae6fa4e5ed228f48b911c0149ce48b1').then( balance => {
    /**
     * Veremos más adelante, pero en ethereum no existen la coma flotante, no existen los números reales, solo enteros
     * entonces se definio que los 18 digitos a la derecha representan la parte decimal.
     * Es decir que cuando consultamos por el balance en moneda nativa de una cuenta y nos da 1000000000000000000,
     * entonces significa que la cuenta tiene 1 ETH.
     */
    const balanceCrudo = balance.toString()
    /**
     * Al ser algo tan general a todo el contexto, ethers nos provee un funcion exclusica para dividir los numeros
     * y llevarlos su forma decimal.
     * Esta función de fondo, divide balance por 10^18.
     */
    const balanceFormateado = ethers.utils.formatUnits(balance, 18)
    console.log('Nativo', {balanceCrudo,balanceFormateado})
})

/**
 * Este balance es con el que se pagan los cambios de estado y los eventos que genera una transacción.
 * Ahora vamos a repasar como un contrato, pero puntualmente con el contrato que represeta los ERC20,
 * los famosos tokens que estan por todos lados, como veremos en las proximas unidades los contratos
 * en general respetan estandares en su interface, metodos y variables.
 * En el siguiente ejemplo vamos a consultar por el balance de una cuenta en un token particular,
 * para esto nos vamos a valer de las herramientas básicas.
 */

/**
 * Vamos a llamar a la función balanceOf de un contrato ERC20, esta función recibe como parámetro un address
 * y retorna su balance actual del token. Este balance está representado por defecto con 18 decimales, salvo que el
 * método decimals de dicho contrato retorne otro valor. Vamos a repasar el funcionamiento
 */
export async function balanceOf(tokenAddress: string, accountAddress: string){
    /**
     * Para llamar a una función de un contrato, se utilizan los primeros 8 bytes del SHA-3 del encabezado de la función.
     * el encabezado es el nombre de la función y los tipos de sus parametros. Por ejemplo:
     * tenemos la función function mint(address to, uint256 amount) public { ... }
     * el encabezado de esa función es mint(address,uint256).
     */
    const functionHash = ethers.utils.solidityKeccak256(["string"], ["balanceOf(address)"])
    /**
     * La siguiente función encodea los parametros segun su tipo en este caso, el único parametro de la función
     * balanceOf, el address de la cuenta que queremos consultar.
     */
    const args = ethers.utils.defaultAbiCoder.encode([ "address" ], [ accountAddress ])

    const data = '0x'+
        functionHash.slice(2,10)+ // Los primeros 4 bytes sin el 0x
        args.slice(2) // Todos los paramentros encodeados sin el 0x
    /**
     * Ahora enviamos al nodo el llamado encodeado y le definimos  la dirección del contrato.
     * La resupuesta del metodo balanceOf(address) es un uint256, esta respuesta la tenemos
     * que decodificar.
     */
    // https://docs.ethers.io/v5/api/providers/provider/#Provider-call
    const result = await provider.call({
        to: tokenAddress,
        data
    })
    return ethers.utils.defaultAbiCoder.decode([ "uint256" ], result )[0]
}


/**
 * Esta es la dirección del token que vamos a usar, el codigo fuente esta en Contratos/contracts/ERC20_ArqBC.sol
 * https://rinkeby.etherscan.io/token/0xa3fc68dce6cf25b4fd8dfdb7245467cc5e13dc18
 */
const tokenAddress = '0xa3fC68dCe6CF25b4fD8DfdB7245467cC5E13DC18'

balanceOf(tokenAddress, '0x87B4f1f9EaE6Fa4E5ED228F48b911c0149Ce48b1').then( balance => {
    const balanceCrudo = balance.toString()
    const balanceFormateado = ethers.utils.formatUnits(balance, 18)
    console.log('Token', {balanceCrudo,balanceFormateado})
})

/**
 * En la siguiente función vamos a ejecutar la funcion dameToken de nuestro contrato de Fondeo básico
 * Contratos/contracts/Faucet_ArqBC.sol. Este contrato puede crear nuevas monedas y dárselas a la cuenta que está
 * firmando la transacción y pagando el gas. Como esta transacción requiere modificaciónes del estado
 * y emisión de eventos, requiere gas para ser procesada en toda la red. Necesitamos que una cuenta con balance positivo
 * en la moneda nativa firme la transacción.
 */
async function dameToken(wallet: ethers.Wallet, faucentAddress: string){
    // ethers.utils.id calcula el sha-3 de un string
    const functionHash = ethers.utils.id("dameToken()")

    const data = '0x' + functionHash.slice(2,10)

    try {
        await wallet.sendTransaction({
            to: faucentAddress,
            data
        })
    } catch (e) {
        /**
         * si la ejecución falla porque no puede estimar el gas, lo más probable es que la transacción dispare
         * un revert porque no se esta cumpliendo una condición requerida o porque la transaccion va a fallar por alguna
         * otra razon. Repasar el codigo de la función en el contrato.
         */
        console.log(e.reason)
    }
}

/**
 * Esta es una instancia de nuestro contrato de Faucet
 * https://rinkeby.etherscan.io/address/0x1ccd6d633d8e10d7f705b6ac618bc2df1639dd56#code
 */
const faucetAddress = '0x1ccd6d633d8e10d7f705b6ac618bc2df1639dd56'
/**
 * TODO: Intenta usar siempre variables de entorno, nunca una privada en el código
 * esta lleno de bots esperando que alguien commitee una por error.
 */
const tuPrivada = process.env.PRIVATE_KEY || ''
const wallet = new ethers.Wallet(tuPrivada)
/**
 * Una wallet por si sola no tiene conexion con la blockchain, para que pueda comunicarse necesitamos conectarla
 * con un proveedor
 */
const walletConectada = wallet.connect(provider)
dameToken(walletConectada, faucetAddress).then( async () => {
    const balance = await balanceOf(tokenAddress, wallet.address)
    const balanceCrudo = balance.toString()
    const balanceFormateado = ethers.utils.formatUnits(balance, 18)
    console.log('Token', {balanceCrudo,balanceFormateado})
})