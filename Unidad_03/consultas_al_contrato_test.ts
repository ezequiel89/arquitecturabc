import * as assert from "assert";
import {BigNumberish, ethers} from "ethers";

/**
 * Las soluciones tiene que ser consultando a los contratos
 * y completando la variable evaluada con la respuesta a un llamado
 */

describe('Interactuar con un contrato', function () {
    const provider = new ethers.providers.InfuraProvider("rinkeby")
    it("¿Cual es el balance del token 0xa3fC68dCe6CF25b4fD8DfdB7245467cC5E13DC18 para la cuenta 0x2a4684F2e9B5F23c43d0BA001F0c06942f27A95A?", async ()=>{
        let respuesta:BigNumberish = 0
        assert.equal(ethers.utils.solidityKeccak256(['uint256'], [respuesta]), "0xeb56b1d3ea8cb70aecb2523bb3f1061affbd3cc2b5d6307a2fafc6a1c1f4c868")
    })
    it("¿Cual es el total circulante del token 0xa3fC68dCe6CF25b4fD8DfdB7245467cC5E13DC18?", async ()=>{
        // En encabezado de la función es totalSupply() y retorna un uint256
        let respuesta:BigNumberish = 0
        assert.equal(ethers.utils.solidityKeccak256(['uint256'],[respuesta]), "0x3a9cdc0a2aec1f46104083ce0e315f4f7122304a34641bf96c72401942c09bd4")
    })
});